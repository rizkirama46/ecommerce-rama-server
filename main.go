package main

import (
	"api-ecommerce-rama/config"
	"api-ecommerce-rama/docs"
	"api-ecommerce-rama/routes"
	"api-ecommerce-rama/utils"
	"log"

	"github.com/joho/godotenv"
)

func main() {
	environment := utils.Getenv("ENVIRONMENT", "development")

	// for load godotenv
	if environment == "development" {
		err := godotenv.Load()
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}

	docs.SwaggerInfo.Title = "API E-Commerce Rama SuperBootcamp"
	docs.SwaggerInfo.Description = "This is a sample server E-Commerce."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = utils.Getenv("SWAGGER_HOST", "localhost:8080")
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	db := config.ConnectDataBase()
	sqlDB, _ := db.DB()
	defer sqlDB.Close()

	r := routes.SetupRouter(db)
	r.Run()
}
