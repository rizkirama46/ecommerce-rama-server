package controllers

import (
	"fmt"
	"net/http"

	"api-ecommerce-rama/models"
	"api-ecommerce-rama/utils/token"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type TransactionInput struct {
	Quantity  uint `json:"quantity"`
	ProductID uint `json:"product_id"`
}

type CheckoutInput struct {
	Total float64 `json:"total"`
}

// GetTransactions godoc
// @Summary Get all Transaction.
// @Description Get a list of Transaction.
// @Tags Transaction
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} []models.Cart
// @Router /transactions [get]
func GetTransactions(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var transactions []models.Transaction
	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	db.Preload("Product").Preload("User").Where("user_id = ?", userID).Find(&transactions)

	c.JSON(http.StatusOK, gin.H{"success": true, "data": transactions})
}

// CreateTransaction godoc
// @Summary Create New Transaction.
// @Description Creating a new Transaction.
// @Tags Transaction
// @Param Body body TransactionInput true "the body to create a new Transaction"
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Transaction
// @Router /transactions [post]
func CreateTransaction(c *gin.Context) {
	// Validate input
	var product models.Product
	var input TransactionInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)

	// Check Stock Product
	if err := db.Where("id = ?", input.ProductID).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	if product.Stock < input.Quantity {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Stock not enough"})
		return
	}

	var updateInputStock models.Product
	updateInputStock.Stock = product.Stock - input.Quantity

	db.Model(&product).Updates(updateInputStock)

	// Create Transaction
	transaction := models.Transaction{Quantity: input.Quantity, ProductID: input.ProductID, UserID: userID}
	db.Create(&transaction)

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success add transaction"})
}

// GetTransactionById godoc
// @Summary Get Transaction.
// @Description Get an Transaction by id.
// @Tags Transaction
// @Produce json
// @Param id path string true "Transaction id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Transaction
// @Router /transactions/{id} [get]
func GetTransactionById(c *gin.Context) { // Get model if exist
	var transaction models.Transaction

	db := c.MustGet("db").(*gorm.DB)
	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	if err := db.Where("id = ? AND user_id = ?", c.Param("id"), userID).First(&transaction).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": transaction})
}

// UpdateTransaction godoc
// @Summary Update Transaction.
// @Description Update Transaction by id.
// @Tags Transaction
// @Produce json
// @Param id path string true "Transaction id"
// @Param Body body TransactionInput true "the body to update Transaction"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Transaction
// @Router /transactions/{id} [put]
func UpdateTransaction(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var transaction models.Transaction
	if err := db.Where("id = ?", c.Param("id")).First(&transaction).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	// Validate input
	var input TransactionInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	var updatedInput models.Transaction
	updatedInput.Quantity = input.Quantity
	updatedInput.ProductID = input.ProductID
	updatedInput.UserID = userID

	db.Model(&transaction).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success add transaction", "data": transaction})
}

// DeleteTransaction godoc
// @Summary Delete one Transaction.
// @Description Delete a Transaction by id.
// @Tags Transaction
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param id path string true "Transaction id"
// @Success 200 {object} map[string]boolean
// @Router /transactions/{id} [delete]
func DeleteTransaction(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var transaction models.Transaction
	if err := db.Where("id = ?", c.Param("id")).First(&transaction).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	db.Delete(&transaction)

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success delete transaction"})
}

// Checkout godoc
// @Summary Checkout.
// @Description Checkout Products.
// @Tags Transaction
// @Param Body body CheckoutInput true "the body to Checkout"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} []models.Cart
// @Router /transactions/checkout [patch]
func Checkout(c *gin.Context) {
	// get user id from token
	userID, err := token.ExtractTokenID(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	// Validate input
	var input CheckoutInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// check balance
	var wallet models.Wallet
	db.Where("user_id = ?", userID).First(&wallet)
	if wallet.Balance < input.Total {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Balance not enough"})
		return
	}

	// create transaction
	var carts []models.Cart
	var transactions []models.Transaction

	db.Where("user_id = ?", userID).Preload("Product").Find(&carts)

	for _, cart := range carts {
		if cart.Quantity > cart.Product.Stock {
			c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": fmt.Sprintf("%s out of stock", cart.Product.Name)})
			return
		}
		transaction := models.Transaction{Quantity: cart.Quantity, ProductID: cart.ProductID, UserID: userID}
		transactions = append(transactions, transaction)
	}

	db.Transaction(func(tx *gorm.DB) error {
		// delete cart
		if err := db.Where("user_id = ?", userID).Delete(&models.Cart{}).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
			return err
		}

		// update stock
		for _, cart := range carts {
			err := db.Model(&models.Product{}).Where("id = ?", cart.ProductID).Update("stock", cart.Product.Stock-cart.Quantity).Error
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
				return err
			}
		}

		// add transaction
		if err := db.Create(&transactions).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
			return err
		}

		// update wallet
		if err := db.Model(&models.Wallet{}).Where("user_id = ?", userID).Update("balance", wallet.Balance-input.Total).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
			return err
		}

		// detail wallet
		detailWallet := models.DetailWallet{Balance: input.Total, Type: 2, WalletID: wallet.ID}
		if err := db.Create(&detailWallet).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
			return err
		}

		return nil
	})

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success checkout", "data": carts})
}
