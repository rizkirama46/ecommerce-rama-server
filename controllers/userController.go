package controllers

import (
	"api-ecommerce-rama/models"
	"api-ecommerce-rama/utils/token"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserInput struct {
	Name    string `json:"name" binding:"required"`
	Phone   string `json:"phone"`
	Address string `json:"address"`
}

type PasswordInput struct {
	CurrentPassword string `json:"current_password" binding:"required"`
	NewPassword     string `json:"new_password" binding:"required"`
}

// GetUsers godoc
// @Summary Get all User.
// @Description Get a list of User.
// @Tags User
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} []models.User
// @Router /users/all [get]
func GetUsers(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var users []models.User
	db.Select("id", "name", "email", "phone", "address", "role").Find(&users)

	c.JSON(http.StatusOK, gin.H{"success": true, "data": users})
}

// GetUserById godoc
// @Summary Get User.
// @Description Get an User by id.
// @Tags User
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.User
// @Router /users [get]
func GetUserById(c *gin.Context) {
	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	// Get model if exist
	var user models.User

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Select("id", "name", "email", "phone", "address", "role").Where("id = ?", userID).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": user})
}

// UpdateUser godoc
// @Summary Update User.
// @Description Update User.
// @Tags User
// @Produce json
// @Param Body body UserInput true "the body to update user"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.User
// @Router /users [put]
func UpdateUser(c *gin.Context) {

	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var user models.User
	if err := db.Where("id = ?", userID).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	// Validate input
	var input UserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	var updatedInput models.User
	updatedInput.Name = input.Name
	updatedInput.Phone = input.Phone
	updatedInput.Address = input.Address

	db.Model(&user).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success update user", "data": user})
}

// ChangePassword godoc
// @Summary Update User Password.
// @Description Update User Password.
// @Tags User
// @Produce json
// @Param Body body PasswordInput true "the body to update password user"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.User
// @Router /users/change-password [patch]
func ChangePassword(c *gin.Context) {

	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var user models.User
	if err := db.Where("id = ?", userID).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	// Validate input
	var input PasswordInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	// check password
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(input.CurrentPassword)); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Current Password is wrong"})
		return
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(input.NewPassword)); err == nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "the password is still the same as before"})
		return
	}

	// hash password
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(input.NewPassword), bcrypt.DefaultCost)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"success": false, "message": err.Error()})
		return
	}

	var updatedInput models.User
	updatedInput.Password = string(hashedPassword)

	db.Model(&user).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success update password"})
}
