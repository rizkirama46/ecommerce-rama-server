package controllers

import (
	"api-ecommerce-rama/models"
	"api-ecommerce-rama/utils/token"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type WalletInput struct {
	Balance float64 `json:"balance"`
	Type    uint    `json:"type"`
}

// GetWalletById godoc
// @Summary Get Wallet.
// @Description Get an Wallet
// @Tags Wallet
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Wallet
// @Router /wallets [get]
func GetWalletById(c *gin.Context) {
	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	// Get model if exist
	var wallet models.Wallet

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("user_id = ?", userID).Preload("DetailWallet").First(&wallet).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": wallet})
}

// UpdateWallet godoc
// @Summary Update Wallet.
// @Description Update Wallet.
// @Tags Wallet
// @Produce json
// @Param Body body WalletInput true "the body to update wallet"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Wallet
// @Router /wallets [put]
func UpdateWallet(c *gin.Context) {

	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var wallet models.Wallet
	if err := db.Where("user_id = ?", userID).First(&wallet).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	// Validate input
	var input WalletInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	var updatedInput models.Wallet

	if input.Type == 1 {
		updatedInput.Balance = wallet.Balance + input.Balance
	} else {
		if wallet.Balance < input.Balance {
			c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Balance is not enough"})
			return
		}
		updatedInput.Balance = wallet.Balance - input.Balance
	}

	detailWallet := models.DetailWallet{Balance: input.Balance, Type: input.Type, WalletID: wallet.ID}

	db.Transaction(func(tx *gorm.DB) error {
		if err := db.Model(&wallet).Updates(updatedInput).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
			return err
		}

		if err := db.Create(&detailWallet).Error; err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
			return err
		}

		return nil
	})

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success update wallet", "data": wallet})
}
