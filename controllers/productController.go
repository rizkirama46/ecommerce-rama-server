package controllers

import (
	"net/http"

	"api-ecommerce-rama/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ProductInput struct {
	Name        string  `json:"product_name"`
	Description string  `json:"description"`
	Price       float64 `json:"price"`
	ImageUrl    string  `json:"image_url"`
	Stock       uint    `json:"stock"`
	Type        string  `json:"type"`
	CategoryID  uint    `json:"category_id"`
}

type ShowBestSeller struct {
	ID       uint
	Name     string
	Price    float64
	ImageUrl string
	Sum      uint
}

// GetProducts godoc
// @Summary Get all Product.
// @Description Get a list of Product.
// @Tags Product
// @Produce json
// @Success 200 {object} []models.Product
// @Router /products [get]
func GetProducts(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var products []models.Product
	db.Preload("Category").Preload("Cart").Find(&products)

	c.JSON(http.StatusOK, gin.H{"success": true, "data": products})
}

// CreateProduct godoc
// @Summary Create New Product.
// @Description Creating a new Product.
// @Tags Product
// @Param Body body ProductInput true "the body to create a new Product"
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Product
// @Router /products [post]
func CreateProduct(c *gin.Context) {
	// Validate input
	var input ProductInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	// Create Product
	product := models.Product{Name: input.Name, Description: input.Description, Price: input.Price, ImageUrl: input.ImageUrl, Stock: input.Stock, Type: input.Type, CategoryID: input.CategoryID}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&product)

	db.Preload("Cart").Preload("Category").Preload("Transaction").Where("id = ?", product.ID).First(&product)

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success add product", "data": product})
}

// GetProductById godoc
// @Summary Get Product.
// @Description Get an Product by id.
// @Tags Product
// @Produce json
// @Param id path string true "Product id"
// @Success 200 {object} models.Product
// @Router /products/{id} [get]
func GetProductById(c *gin.Context) { // Get model if exist
	var product models.Product

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).Preload("Category").First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": product})
}

// UpdateProduct godoc
// @Summary Update Product.
// @Description Update Product by id.
// @Tags Product
// @Produce json
// @Param id path string true "Product id"
// @Param Body body ProductInput true "the body to update Product"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Product
// @Router /products/{id} [put]
func UpdateProduct(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var product models.Product
	if err := db.Where("id = ?", c.Param("id")).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	// Validate input
	var input ProductInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	var updatedInput models.Product
	updatedInput.Name = input.Name
	updatedInput.Description = input.Description
	updatedInput.Price = input.Price
	updatedInput.ImageUrl = input.ImageUrl
	updatedInput.Stock = input.Stock
	updatedInput.Type = input.Type
	updatedInput.CategoryID = input.CategoryID

	db.Model(&product).Updates(updatedInput)

	db.Preload("Cart").Preload("Category").Preload("Transaction").Where("id = ?", product.ID).First(&product)

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success update product", "data": product})
}

// DeleteProduct godoc
// @Summary Delete one Product.
// @Description Delete a Product by id.
// @Tags Product
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param id path string true "Product id"
// @Success 200 {object} map[string]boolean
// @Router /products/{id} [delete]
func DeleteProduct(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var product models.Product
	if err := db.Where("id = ?", c.Param("id")).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	db.Delete(&product)

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success delete product"})
}

// GetTopProducts godoc
// @Summary Get all Top Product.
// @Description Get a list of Top Product.
// @Tags Product
// @Produce json
// @Success 200 {object} []models.Product
// @Router /products/top [get]
func GetTopProducts(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var products []models.Product
	db.Where("type = ?", "top").Preload("Category").Preload("Transaction").Find(&products)

	c.JSON(http.StatusOK, gin.H{"success": true, "data": products})
}

// GetBottomProducts godoc
// @Summary Get all Bottom Product.
// @Description Get a list of Bottom Product.
// @Tags Product
// @Produce json
// @Success 200 {object} []models.Product
// @Router /products/bottom [get]
func GetBottomProducts(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var products []models.Product
	db.Where("type = ?", "bottom").Preload("Category").Preload("Transaction").Find(&products)

	c.JSON(http.StatusOK, gin.H{"success": true, "data": products})
}

// GetAccessoriesProducts godoc
// @Summary Get all Accessories Product.
// @Description Get a list of Accessories Product.
// @Tags Product
// @Produce json
// @Success 200 {object} []models.Product
// @Router /products/accessories [get]
func GetAccessoriesProducts(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var products []models.Product
	db.Where("type = ?", "accessories").Preload("Category").Preload("Transaction").Find(&products)

	c.JSON(http.StatusOK, gin.H{"success": true, "data": products})
}
