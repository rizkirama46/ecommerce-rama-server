package controllers

import (
	"fmt"
	"html"
	"net/http"
	"strings"

	"api-ecommerce-rama/models"
	"api-ecommerce-rama/utils/token"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type RegisterInput struct {
	Name     string `json:"name" binding:"required"`
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
	Phone    string `json:"phone"`
	Address  string `json:"address"`
}

type LoginInput struct {
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// Register godoc
// @Summary Register an user.
// @Description Creating a new Account.
// @Tags Auth
// @Param Body body RegisterInput true "the body to create a new User"
// @Produce json
// @Success 200 {object} models.User
// @Router /register [post]
func Register(c *gin.Context) {
	// Validate input
	var input RegisterInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// hash password
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"success": false, "message": err.Error()})
		return
	}

	// Create User
	register := models.User{}
	register.Name = html.EscapeString(strings.TrimSpace(input.Name))
	register.Email = input.Email
	register.Password = string(hashedPassword)
	register.Phone = input.Phone
	register.Address = input.Address
	register.Role = "user"

	db := c.MustGet("db").(*gorm.DB)

	db.Transaction(func(tx *gorm.DB) error {
		if err := tx.Create(&register).Error; err != nil {
			return err
		}

		// Create Wallet
		wallet := models.Wallet{Balance: 0, UserID: register.ID}
		if err := tx.Create(&wallet).Error; err != nil {
			return err
		}

		return nil
	})

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "register success", "data": register})
}

// LoginUser godoc
// @Summary Login as as user.
// @Description Logging in to get jwt token to access admin or user api by roles.
// @Tags Auth
// @Param Body body LoginInput true "the body to login a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /login [post]
func Login(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input LoginInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	u := models.User{}

	u.Email = input.Email
	u.Password = input.Password

	// check email
	err := db.Model(models.User{}).Where("email = ?", u.Email).Preload("Wallet").First(&u).Error
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "email or password is incorrect."})
		return
	}

	// check password
	err = bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(input.Password))
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "email or password is incorrect."})
		return
	}

	token, err := token.GenerateToken(u.ID)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{"success": false, "message": "internal server error."})
		return
	}

	var user map[string]string

	if u.Role == "user" {
		user = map[string]string{
			"username": u.Name,
			"email":    u.Email,
			"role":     u.Role,
			"balance":  fmt.Sprintf("%f", u.Wallet[0].Balance),
		}
	} else {
		user = map[string]string{
			"username": u.Name,
			"email":    u.Email,
			"role":     u.Role,
		}
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "login success", "user": user, "token": token})
}

// RegisterAdmin godoc
// @Summary Register an admin.
// @Description Creating a new Account.
// @Tags Auth
// @Param Body body RegisterInput true "the body to create a new Admin"
// @Produce json
// @Success 200 {object} models.User
// @Router /register/admin [post]
func RegisterAdmin(c *gin.Context) {
	// Validate input
	var input RegisterInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// hash password
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"success": false, "message": err.Error()})
		return
	}

	// Create User
	register := models.User{}
	register.Name = html.EscapeString(strings.TrimSpace(input.Name))
	register.Email = input.Email
	register.Password = string(hashedPassword)
	register.Phone = input.Phone
	register.Address = input.Address
	register.Role = "admin"

	db := c.MustGet("db").(*gorm.DB)
	db.Create(&register)

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "register success"})
}
