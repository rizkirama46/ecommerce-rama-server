package controllers

import (
	"net/http"

	"api-ecommerce-rama/models"
	"api-ecommerce-rama/utils/token"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type CartInput struct {
	Quantity  uint `json:"quantity"`
	ProductID uint `json:"product_id"`
}

// GetCart godoc
// @Summary Get all Cart.
// @Description Get a list of Cart.
// @Tags Cart
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} []models.Cart
// @Router /carts [get]
func GetCart(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var carts []models.Cart
	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	db.Preload("Product").Preload("User").Where("user_id = ?", userID).Find(&carts)

	c.JSON(http.StatusOK, gin.H{"success": true, "data": carts})
}

// CreateCart godoc
// @Summary Create New Cart.
// @Description Creating a new Cart.
// @Tags Cart
// @Param Body body CartInput true "the body to create a new Cart"
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Cart
// @Router /carts [post]
func CreateCart(c *gin.Context) {
	// Validate input
	var input CartInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)

	// Create Cart
	cart := models.Cart{Quantity: input.Quantity, ProductID: input.ProductID, UserID: userID}
	db.Create(&cart)

	db.Preload("Product").Preload("User").Where("id = ?", cart.ID).First(&cart)

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success add Cart", "data": cart})
}

// GetCartById godoc
// @Summary Get Cart.
// @Description Get an Cart by id.
// @Tags Cart
// @Produce json
// @Param id path string true "Cart id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Cart
// @Router /carts/{id} [get]
func GetCartById(c *gin.Context) { // Get model if exist
	var cart models.Cart

	db := c.MustGet("db").(*gorm.DB)
	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	if err := db.Where("id = ? AND user_id = ?", c.Param("id"), userID).First(&cart).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "data": cart})
}

// UpdateCart godoc
// @Summary Update Cart.
// @Description Update Cart by id.
// @Tags Cart
// @Produce json
// @Param id path string true "Cart id"
// @Param Body body CartInput true "the body to update Cart"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Cart
// @Router /carts/{id} [put]
func UpdateCart(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var cart models.Cart
	if err := db.Where("id = ?", c.Param("id")).First(&cart).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	// Validate input
	var input CartInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": err.Error()})
		return
	}

	_, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	var updatedInput models.Cart
	updatedInput.Quantity = input.Quantity

	db.Model(&cart).Updates(updatedInput)

	db.Preload("Product").Preload("User").Where("id = ?", cart.ID).First(&cart)

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success update cart", "data": cart})
}

// DeleteCart godoc
// @Summary Delete one Cart.
// @Description Delete a Cart by id.
// @Tags Cart
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param id path string true "Cart id"
// @Success 200 {object} map[string]boolean
// @Router /carts/{id} [delete]
func DeleteCart(c *gin.Context) {
	// Get model if exist
	userID, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Invalid token"})
		return
	}

	db := c.MustGet("db").(*gorm.DB)

	var cart models.Cart

	if err := db.Where("id = ? AND user_id = ?", c.Param("id"), userID).First(&cart).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "Record not found!"})
		return
	}

	db.Delete(&cart)

	c.JSON(http.StatusOK, gin.H{"success": true, "message": "success delete cart"})
}
