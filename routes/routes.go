package routes

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"api-ecommerce-rama/controllers"
	"api-ecommerce-rama/middlewares"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()
	r.SetTrustedProxies([]string{"localhost"})

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowHeaders = []string{"Content-Type", "X-XSRF-TOKEN", "Accept", "Origin", "X-Requested-With", "Authorization"}

	// To be able to send tokens to the server.
	corsConfig.AllowCredentials = true
	// OPTIONS method for ReactJS
	corsConfig.AddAllowMethods("OPTIONS")

	r.Use(cors.New(corsConfig))

	// set db to gin context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	// auth routes
	r.POST("/register", controllers.Register)
	r.POST("/register/admin", controllers.RegisterAdmin)
	r.POST("/login", controllers.Login)

	// user routes
	userMiddleware := r.Group("/users")
	userMiddleware.Use(middlewares.JwtAuthMiddleware())
	userMiddleware.GET("/all", controllers.GetUsers)
	userMiddleware.GET("", controllers.GetUserById)
	userMiddleware.PUT("", controllers.UpdateUser)
	userMiddleware.PATCH("/change-password", controllers.ChangePassword)

	// category routes
	r.GET("/categories", controllers.GetCategories)
	r.GET("/categories/:id", controllers.GetCategoryById)

	categoryMiddleware := r.Group("/categories")
	categoryMiddleware.Use(middlewares.JwtAuthMiddleware())
	categoryMiddleware.POST("", controllers.CreateCategory)
	categoryMiddleware.PUT("/:id", controllers.UpdateCategory)
	categoryMiddleware.DELETE("/:id", controllers.DeleteCategory)

	// product routes
	r.GET("/products", controllers.GetProducts)
	r.GET("/products/:id", controllers.GetProductById)
	r.GET("/products/top", controllers.GetTopProducts)
	r.GET("/products/bottom", controllers.GetBottomProducts)
	r.GET("/products/accessories", controllers.GetAccessoriesProducts)

	productMiddleware := r.Group("/products")
	productMiddleware.Use(middlewares.JwtAuthMiddleware())
	productMiddleware.POST("", controllers.CreateProduct)
	productMiddleware.PUT("/:id", controllers.UpdateProduct)
	productMiddleware.DELETE("/:id", controllers.DeleteProduct)

	// transaction routes
	cartMiddleware := r.Group("/carts")
	cartMiddleware.Use(middlewares.JwtAuthMiddleware())
	cartMiddleware.GET("", controllers.GetCart)
	cartMiddleware.GET("/:id", controllers.GetCartById)
	cartMiddleware.POST("", controllers.CreateCart)
	cartMiddleware.PUT("/:id", controllers.UpdateCart)
	cartMiddleware.DELETE("/:id", controllers.DeleteCart)

	// transaction routes
	transactionMiddleware := r.Group("/transactions")
	transactionMiddleware.Use(middlewares.JwtAuthMiddleware())
	transactionMiddleware.GET("", controllers.GetTransactions)
	transactionMiddleware.GET("/:id", controllers.GetTransactionById)
	transactionMiddleware.POST("", controllers.CreateTransaction)
	transactionMiddleware.PUT("/:id", controllers.UpdateTransaction)
	transactionMiddleware.DELETE("/:id", controllers.DeleteTransaction)
	transactionMiddleware.PATCH("/checkout", controllers.Checkout)

	walletMiddleware := r.Group("/wallets")
	walletMiddleware.Use(middlewares.JwtAuthMiddleware())
	walletMiddleware.GET("", controllers.GetWalletById)
	walletMiddleware.PUT("", controllers.UpdateWallet)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
