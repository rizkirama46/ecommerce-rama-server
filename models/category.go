package models

type Category struct {
	ID      uint   `gorm:"primary_key" json:"category_id"`
	Name    string `gorm:"not null;unique" json:"category_name"`
	Product []Product
}
