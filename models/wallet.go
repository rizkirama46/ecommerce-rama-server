package models

type Wallet struct {
	ID           uint    `gorm:"primary_key" json:"wallet_id"`
	Balance      float64 `gorm:"not null;" json:"balance"`
	UserID       uint    `gorm:"not null;unique" json:"user_id"`
	User         User    `gorm:"foreignkey:UserID"`
	DetailWallet []DetailWallet
}

type DetailWallet struct {
	ID       uint    `gorm:"primary_key" json:"detail_wallet_id"`
	Balance  float64 `gorm:"not null;" json:"balance"`
	Type     uint    `gorm:"not null;" json:"type"`
	WalletID uint    `gorm:"not null;" json:"wallet_id"`
	Wallet   Wallet  `gorm:"foreignkey:WalletID"`
}
