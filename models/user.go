package models

type User struct {
	ID          uint          `gorm:"primary_key" json:"user_id"`
	Name        string        `gorm:"not null;unique" json:"name"`
	Email       string        `gorm:"not null;unique" json:"email"`
	Password    string        `gorm:"not null;" json:"password"`
	Phone       string        `json:"phone"`
	Address     string        `json:"address"`
	Role        string        `json:"role"`
	Cart        []Cart        `json:"-"`
	Transaction []Transaction `json:"-"`
	Wallet      []Wallet
}
