package models

type Product struct {
	ID          uint     `gorm:"primary_key" json:"product_id"`
	Name        string   `gorm:"not null;" json:"product_name"`
	Description string   `json:"description"`
	Price       float64  `gorm:"not null;" json:"price"`
	ImageUrl    string   `json:"image_url"`
	Type        string   `gorm:"not null;" json:"type"`
	Stock       uint     `gorm:"not null;" json:"stock"`
	CategoryID  uint     `json:"category_id"`
	Category    Category `gorm:"foreignkey:CategoryID"`
	Cart        []Cart
	Transaction []Transaction
}
