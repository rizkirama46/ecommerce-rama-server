package models

type Transaction struct {
	ID        uint    `gorm:"primary_key" json:"transaction_id"`
	Quantity  uint    `gorm:"not null;" json:"quantity"`
	ProductID uint    `gorm:"not null;" json:"product_id"`
	UserID    uint    `gorm:"not null;" json:"user_id"`
	Product   Product `gorm:"foreignkey:ProductID"`
	User      User    `gorm:"foreignkey:UserID"`
}
